FROM  python:3.8.1-buster

COPY f1db_csv /streamlit/f1db_csv
COPY app.py /streamlit/app.py
COPY requirements.txt /streamlit/requirements.txt
COPY .streamlit   /streamlit/.streamlit

WORKDIR /streamlit

RUN apt-get update && \
    apt-get upgrade

RUN apt-get -y install build-essential libssl-dev libffi-dev python3-dev && \    
    pip3 install -r requirements.txt

CMD streamlit run app.py