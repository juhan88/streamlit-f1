resource "aws_security_group" "container" {
  name        = "${local.app.name}"
  description = "streamlit-container-sg"
  vpc_id      = local.vpc.id

  ingress {
    # TLS (change to whatever ports you need)
    from_port   = 8501
    to_port     = 8501
    protocol    = "tcp"    
    cidr_blocks = ["0.0.0.0/0"]
  }

   ingress {
    # TLS (change to whatever ports you need)
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"    
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port       = 0
    to_port         = 0
    protocol        = "-1"
    cidr_blocks     = ["0.0.0.0/0"]    
  }
}
