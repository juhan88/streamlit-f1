terraform {
  backend "s3" {
    bucket = "sudobucks-tf-states"
    key    = "portfolio/streamlit/main"
    region = "us-west-2"
  }
}