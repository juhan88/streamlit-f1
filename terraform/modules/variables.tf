locals {
    avail_zones = ["us-west-2a", "us-west-2b", "us-west-2c"]
    app = {
        name    = "streamlit"
        port    = 8501
        cluster = "sudobucks"
        count   = 1
    }
    cert = "arn:aws:acm:us-west-2:413136864600:certificate/1b6c5e33-879a-4d57-941e-15f6210b67b8"
    cpu  = 256
    memory = 512
    iam = {
        role = "arn:aws:iam::413136864600:role/AWSExecuteRoleForECS"
    }
    launch_type = "FARGATE"
    region      = "us-west-2"
    vpc = {
        subnets = [ "subnet-02cd9844", "subnet-089a5323", "subnet-53694327", "subnet-b3e2ead1" ]
        id      = "vpc-1aaba378"
    }      
}