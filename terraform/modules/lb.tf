resource "aws_lb" "portfolio" {
  name               = "entry-portfolio"
  internal           = false
  load_balancer_type = "application"
  security_groups    = [ aws_security_group.container.id ]
  subnets            = local.vpc.subnets

  enable_deletion_protection = true

  tags = {
    Environment = "production"
  }
}

resource "aws_lb_target_group" "streamlit" {
  name     = "${local.app.name}-entry"
  port     = local.app.port
  protocol = "HTTP"
  target_type = "ip"
  vpc_id   = local.vpc.id
  health_check {
    path = "/f1/healthz"
  }
}

resource "aws_lb_listener" "entry" {
  load_balancer_arn = aws_lb.portfolio.arn
  port              = "443"
  protocol          = "HTTPS"
  ssl_policy        = "ELBSecurityPolicy-2016-08"
  certificate_arn   = local.cert

  default_action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.streamlit.arn
  }
}