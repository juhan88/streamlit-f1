### Create ECR Repo for container image
resource "aws_ecr_repository" "streamlit" {
  name                 = local.app.name
  image_tag_mutability = "MUTABLE"

  image_scanning_configuration {
    scan_on_push = true
  }
}

### Create ECS Cluster
resource "aws_ecs_cluster" "sudobucks" {
  name = local.app.cluster
}

### Create CloudWatch Log Group for Fargate
resource "aws_cloudwatch_log_group" "streamlit" {
  name = local.app.name
}

### Use Container Definition defined by CloudPosse instead of using static json
### https://registry.terraform.io/modules/cloudposse/ecs-container-definition/aws/0.15.0
module "container-definition" {
  source           = "cloudposse/ecs-container-definition/aws"
  version          = "0.15.0"
  container_cpu    = local.cpu
  container_memory = local.memory
  container_name   = local.app.name
  container_image  = "${aws_ecr_repository.streamlit.repository_url}:latest"
  log_options      = { 
                      "awslogs-group": local.app.name, 
                      "awslogs-region": local.region, 
                      "awslogs-stream-prefix": local.app.name 
                    }
  port_mappings    = [ { 
                        "containerPort": local.app.port, 
                        "hostPort": local.app.port, 
                        "protocol": "tcp" 
                    } ]
}

### Create Task definition to be consumed by Service
resource "aws_ecs_task_definition" "streamlit" {
  family                    = local.app.name
  container_definitions     = module.container-definition.json
  cpu                       = local.cpu
  execution_role_arn        = local.iam.role
  memory                    = local.memory
  network_mode              = "awsvpc"
  requires_compatibilities  = [local.launch_type]
}

resource "aws_ecs_service" "streamlit" {
  name            = local.app.name
  cluster         = local.app.cluster
  task_definition = aws_ecs_task_definition.streamlit.arn
  desired_count   = local.app.count
  launch_type     = local.launch_type

  load_balancer {
    target_group_arn = aws_lb_target_group.streamlit.arn
    container_name   = local.app.name
    container_port   = local.app.port
  }

  network_configuration {
    subnets          = local.vpc.subnets
    security_groups  = [ aws_security_group.container.id ]
    assign_public_ip = true
  }
}


output "container-def" { value = module.container-definition.json }
output "task-def-arn" { value = aws_ecs_task_definition.streamlit.arn }