import streamlit as st
import pandas as pd
import numpy as np
import plotly.express as px
import plotly.graph_objects as go
import statistics


def render_headers():
    st.title('Visualizaing F1 Pitstops.')

    st.markdown('''
    As an avid Formula 1 fan, I attened my first [Grand Prix in Montreal, 2011](https://en.wikipedia.org/wiki/2011_Canadian_Grand_Prix)
    [[YouTube](https://www.youtube.com/watch?v=q6Iaz1-8VC0)], 
    one of the wettest races in history. On the day of the race I had no idea 
    what was going on. But looking back, and watching Jenson Button persevering 
    despite being involved in multiple collisons causing him to fall into last place. 
    Getting a 10s drive through penalty for speeding under safety car, he managed to 
    captilized on Vettel''s mistake on the last lap. 
    ''')
    st.markdown('''
        <img src="https://secure.i.telegraph.co.uk/multimedia/archive/01919/button_1919146c.jpg" style="margin:auto;display: block"/>
        <p style="margin: auto; display: block;text-align: center;">Button winning the wild Canadian GP 2011. Credit: Telegraph.co.uk</p>
        ''', unsafe_allow_html=True)
    
    st.markdown('''
    #### Pit stop : a stop at the pits during an automobile race.
    Data referened from [Ergast Developer API](https://ergast.com/mrd/).
    Pit stop data only begins from the year *2011*. If you have pit stop available prior to 2011 to share, please feel free contact [me](mailto:jha32@sfu.ca).  
    Possible pit stop scenarios:
     - Drive throught penalty (Stop-go) [5s, 10s]
     - Changing Tyres
     - Perform minor adjustments or repairs (Front Wing, Rear Wing, Head-rest)
     - Refuelling (prior 2010) not a factor with current dataset 
    ''')
render_headers()


class all_data(object):
    pit_stops = []
    races     = []
    drivers   = []
    results   = []

# @st.cache
def load_data(nrows):
    folder    = './f1db_csv'                      #TODO: Store in S3
    PIT_STOPS = folder + '/pit_stops.csv'
    RACES     = folder + '/races.csv'
    DRIVERS   = folder + '/driver.csv'
    RESULTS   = folder + '/results.csv'
    LAP_TIMES = folder + '/lap_times.csv'

    data = all_data()
    data.pit_stops = pd.read_csv(PIT_STOPS, index_col=False)
    data.races     = pd.read_csv(RACES)
    data.drivers   = pd.read_csv(DRIVERS, index_col='driverId')
    data.results   = pd.read_csv(RESULTS, index_col='resultId')
    data.lap_times = pd.read_csv(LAP_TIMES)
    return data

data = load_data(10000)


###########                                            Helper Funcs
###################################################################
def collect_from(from_array,value_array):
    result = []
    for value in value_array:
        result.append(from_array.loc[value])
    return result


###########                                         Sidebar Filters
###################################################################
selected_year = st.sidebar.selectbox(
            'Select the Championship Year.',
            range(2011,2019),
            0)
selected_races = data.races.loc[data.races['year'] == selected_year]

############  GrandPrix Filter 
def grandprix_name_format(str):
    return str[4]
selected_grandprix = st.sidebar.selectbox(
            'Select a Grand Prix - ' + str(len(selected_races)),
            selected_races.to_numpy(),
            0,
            grandprix_name_format
            )
selected_pit_stops = data.pit_stops[data.pit_stops['raceId'] == selected_grandprix[0]]
selected_grandprix = pd.DataFrame([selected_grandprix],columns=selected_races.columns)   #Convert back to pd

############  Driver Filter
selected_drivers_index = selected_pit_stops['driverId'].drop_duplicates().to_numpy()
matched_drivers = collect_from(data.drivers,selected_drivers_index)
def driver_name_format(str):    
    return str['displayName']
selected_drivers = st.sidebar.multiselect(
            'Filter by Driver',
            matched_drivers,
            [],
            driver_name_format
            )

selected_drivers = pd.DataFrame(selected_drivers)
### Update selected_pit_stops
if(len(selected_drivers) > 0):
    filtered_pit_stops = pd.DataFrame()
    for index,row in selected_drivers.iterrows():
        filtered_pit_stops = filtered_pit_stops.append(selected_pit_stops[selected_pit_stops['driverId'] == index])
    selected_pit_stops = filtered_pit_stops


###########################################################################
#########################                        Pit stop distribution plot

############# Pit Stop Duration Filter
# pitstop_ms = selected_pit_stops['ms']
# pitstop_min = min(pitstop_ms)/1000
# pitstop_max = max(pitstop_ms)/1000

# st.sidebar.text('Select a range of pit stop duration.')
# selected_v = st.sidebar.slider('Complete pit stops duration in seconds.', 
#                         pitstop_min, 
#                         pitstop_max, 
#                         [pitstop_min,pitstop_max]                        
#                       )

# selected_range = range(int(selected_v[0]*1000),int(selected_v[1]*1000))

###########################################################################
#########################                             Pit stop scatter plot

df_matched_drivers = pd.DataFrame(matched_drivers)
st.subheader('Scatter plot of pit stops')
scatter_plot = go.Figure(data=go.Scatter(
    x = selected_pit_stops['duration'],    
    mode = 'markers',
    marker = dict(
        size=10,
        color=selected_pit_stops['driverId'],
        colorscale='Viridis',
        
    ),
    text = selected_pit_stops['driverId']
))
scatter_plot.update_layout(    
    xaxis_title = 'seconds',
    yaxis_title = 'pitstop'
)
st.plotly_chart(scatter_plot)


#### Merge Data
st.subheader('Pit stop detailed data')

merge_stops_with_drivers = pd.merge(selected_pit_stops,pd.DataFrame(matched_drivers), left_on='driverId', right_index=True)
results = pd.merge(merge_stops_with_drivers,selected_grandprix, left_on='raceId', right_on='raceId' )
if(len(selected_drivers) > 0):     
    filtered_results = pd.DataFrame()    
    for index,row in selected_drivers.iterrows():       
       filtered_results = filtered_results.append(results.loc[results['displayName'] == row['displayName']])  
    st.write(filtered_results[['firstName','lastName','stop','lap','duration','year' ,'name','date']])
    st.sidebar.text('Number of pit stops made: ' + str(len(filtered_results)))
else:
    st.write(results[['firstName','lastName','stop','lap','duration','year' ,'name','date']])
    st.sidebar.text('Number of pit stops made: ' + str(len(results)))

######

###### Get Min and Max from Merged Data
results_duration_raw = results['duration'].to_numpy()
fastest_pitstop = min(results_duration_raw)
slowest_pitstop = max(results_duration_raw)


# st.write(results[results['duration'] == fastest_pitstop])
# st.write(results[results['duration'] == slowest_pitstop])


###########################################################################
#########################                             Lap time scatter plot
selected_grandprix_id_arr = selected_grandprix['raceId'].to_numpy()
selected_lap_times = data.lap_times[data.lap_times['raceId'] == selected_grandprix_id_arr[0] ]

st.subheader('Race Progress')
line_plot = go.Figure(data=go.Scatter())
line_plot.update_layout(    
    xaxis_title = 'lap',
    yaxis_title = 'position',
)

if(len(selected_drivers) > 0):    
    df_selected_drivers_index = selected_drivers.index
    for i in range(len(df_selected_drivers_index)):
        driverId = df_selected_drivers_index[i]
        driverName = selected_drivers.iloc[i]['lastName'] + ', ' + selected_drivers.iloc[i]['firstName']
        lap_times = selected_lap_times[selected_lap_times['driverId'] == driverId ]
        line_plot.add_trace(go.Scatter(
            x=lap_times['lap'],
            y=lap_times['position'],
            mode='lines+markers',
            line_shape='spline',
            name=driverName
        ))
else:
    df_matched_drivers       = pd.DataFrame(matched_drivers)
    df_matched_drivers_index = df_matched_drivers.index
    for i in range(len(df_matched_drivers_index)):
        driverId = df_matched_drivers_index[i]
        driverName = df_matched_drivers.iloc[i]['lastName'] + ', ' + df_matched_drivers.iloc[i]['firstName']
        
        lap_times = selected_lap_times[selected_lap_times['driverId'] == driverId ]
        line_plot.add_trace(go.Scatter(
            x=lap_times['lap'],
            y=lap_times['position'],
            mode='lines+markers',
            name=driverName
        ))

line_plot.update_yaxes(autorange="reversed")
st.plotly_chart(line_plot)
###########                                              Basic Info
###################################################################

# print(type(selected_grandprix.iloc[0]['raceId']))
# print(selected_grandprix['raceId'].iloc[0])

# print(data.results.loc[data.results['raceId'] == selected_grandprix['raceId'].iloc[0]])




###########                                              Todo Block Text
########################################################################

def render_footers():
    st.markdown('''
    Future todo list that would be helpful to understand how the race turned out
    - deduce basic information from data (ie. Retirement, PitStop etc.)
    - weather
    - track data overlayed on top of Google Maps (or Track Image)
    - pit stop detailed data (penalties)
    - how do these things affected the outcome
    
    ''')

render_footers()